import garage_admin_sdk
from garage_admin_sdk.apis import *
from garage_admin_sdk.models import *

configuration = garage_admin_sdk.Configuration(
  host = "http://localhost:3903/v1",
  access_token = "s3cr3t"
)

# Init APIs
api = garage_admin_sdk.ApiClient(configuration)
nodes, layout, keys, buckets = NodesApi(api), LayoutApi(api), KeyApi(api), BucketApi(api)

# Display some info on the node
status = nodes.get_nodes()
print(f"running garage {status.garage_version}, node_id {status.node}")

# Change layout of this node
current = layout.get_layout()
layout.add_layout([
  NodeRoleChange(
    id = status.node,
    zone = "dc1",
    capacity = 1000000000,
    tags = [ "dev" ],
  )
])
layout.apply_layout(LayoutVersion(
  version = current.version + 1
))

# Create key, allow it to create buckets
kinfo = keys.add_key(AddKeyRequest(name="openapi"))

allow_create = UpdateKeyRequestAllow(create_bucket=True)
keys.update_key(kinfo.access_key_id, UpdateKeyRequest(allow=allow_create))

# Create a bucket, allow key, set quotas
binfo = buckets.create_bucket(CreateBucketRequest(global_alias="documentation"))
binfo = buckets.allow_bucket_key(AllowBucketKeyRequest(
  bucket_id=binfo.id,
  access_key_id=kinfo.access_key_id,
  permissions=AllowBucketKeyRequestPermissions(read=True, write=True, owner=True),
))
binfo = buckets.update_bucket(binfo.id, UpdateBucketRequest(
  quotas=UpdateBucketRequestQuotas(max_size=19029801,max_objects=1500)))

# Display key
print(f"""
cluster ready
key id is {kinfo.access_key_id}
secret key is {kinfo.secret_access_key}
bucket {binfo.global_aliases[0]} contains {binfo.objects}/{binfo.quotas.max_objects} objects
""")
