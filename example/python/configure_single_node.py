import garage_admin_sdk
from garage_admin_sdk.apis import *
from garage_admin_sdk.models import *

configuration = garage_admin_sdk.Configuration(
    host = "http://localhost:3903/v1",
    access_token = "s3cr3t"
)

with garage_admin_sdk.ApiClient(configuration) as api_client:
    # --- NODE INFO ---
    nodes = NodesApi(api_client)
    status = nodes.get_nodes()
    print(f"running garage {status.garage_version}, node_id {status.node}")

    # --- LAYOUT ---
    layout = LayoutApi(api_client)

    current = layout.get_layout()
    dirty_node = len(current.roles) != 0 or \
        len(current.staged_role_changes) != 0

    if dirty_node:
        print("this node is dirty, we don't know how to configure it, exiting...")
        exit(1)
    print("it seems to be a fresh node, continuing the configuration")

    layout.add_layout([
        NodeRoleChange(
            id = status.node,
            zone = "dc1", 
            capacity = 1000000000,
            tags = [ "dev" ],
        )
    ])
    print("added myself to the layout")

    current = layout.get_layout()
    assert(len(current.staged_role_changes) == 1)
    print(f"change has been staged successfully: {current.staged_role_changes}")

    newl = LayoutVersion(version = current.version + 1)
    layout.apply_layout(layout_version = newl)
    print(f"new layout has been applied")

    current = layout.get_layout()
    assert(len(current.roles) == 1)
    print(f"new layout is live: {current.roles}")


    # --- CREATE KEY ---
    keys = KeyApi(api_client)
    kreq = AddKeyRequest(name="openapi")
    kinfo = keys.add_key(kreq)
    print(f"key {kinfo.name} added.")

    # --- UPDATE KEY ---
    # not required here, just to demo the API
    kinfo = keys.get_key(search="openapi")

    allow_create = UpdateKeyRequestAllow(create_bucket=True)
    kreq = UpdateKeyRequest(allow=allow_create)
    keys.update_key(kinfo.access_key_id, kreq)
    print(f"key {kinfo.access_key_id} can now create buckets")

    # update key info
    kinfo = keys.get_key(search="openapi", show_secret_key="true")
    print(f"key permissions: {kinfo.permissions}")

    # ---- THE END ---
    print(f"""
Use your key as follow now:

export AWS_ACCESS_KEY_ID={kinfo.access_key_id}
export AWS_SECRET_ACCESS_KEY={kinfo.secret_access_key}
export AWS_REGION=garage

aws --endpoint http://localhost:3900 s3 mb s3://hello-world
aws --endpoint http://localhost:3900 s3 ls
""")
