package main

import (
    "context"
    "fmt"
    "os"
    "strings"
    garage "git.deuxfleurs.fr/garage-sdk/garage-admin-sdk-golang"
)

func main() {
    // Set Host and other parameters
    configuration := garage.NewConfiguration()
    configuration.Host = "127.0.0.1:3903"
    

    // We can now generate a client
    client := garage.NewAPIClient(configuration)

    // Authentication is handled through the context pattern
    ctx := context.WithValue(context.Background(), garage.ContextAccessToken, "s3cr3t")

    // Send a request
    fmt.Println("--- nodes ---")
    resp, r, err := client.NodesApi.GetNodes(ctx).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NodesApi.GetNodes`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }

    // Process the response
    fmt.Fprintf(os.Stdout, "First hostname: %v\n", resp.KnownNodes[0].Hostname)

    // Layout
    capa := int64(1000000000)
    change := []garage.NodeRoleChange{
	garage.NodeRoleChange{NodeRoleUpdate: &garage.NodeRoleUpdate {
	    Id: *resp.KnownNodes[0].Id,
	    Zone: "dc1",
	    Capacity: *garage.NewNullableInt64(&capa),
	    Tags: []string{ "fast", "amd64" },
	}},
    }
    staged, r, err := client.LayoutApi.AddLayout(ctx).NodeRoleChange(change).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LayoutApi.AddLayout`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    msg, r, err := client.LayoutApi.ApplyLayout(ctx).LayoutVersion(*garage.NewLayoutVersion(staged.Version + 1)).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LayoutApi.ApplyLayout`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf(strings.Join(msg.Message, "\n"))

    // Check health
    health, r, err := client.NodesApi.GetHealth(ctx).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NodesApi.GetHealth`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("Status: %s\n", health.Status)
    fmt.Printf("All nodes: %v/%v healthy\n", health.ConnectedNodes, health.KnownNodes)
    fmt.Printf("Storage nodes: %v/%v healthy\n", health.StorageNodesOk, health.StorageNodes)
    fmt.Printf("Partitions: %v/%v healthy\n", health.PartitionsAllOk, health.Partitions)


    // Key
    fmt.Println("\n--- key ---")
    // -- create
    key := "openapi-key"
    keyInfo, r, err := client.KeyApi.AddKey(ctx).AddKeyRequest(garage.AddKeyRequest{Name: *garage.NewNullableString(&key) }).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `KeyApi.AddKey`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("AWS_ACCESS_KEY_ID=%s\nAWS_SECRET_ACCESS_KEY=%s\n", *keyInfo.AccessKeyId, *keyInfo.SecretAccessKey.Get())

    // -- delete
    defer func() {
        r, err := client.KeyApi.DeleteKey(ctx).Id(*keyInfo.AccessKeyId).Execute()
    	if err != nil {
            fmt.Fprintf(os.Stderr, "Error when calling `KeyApi.DeleteKey`\n")
            fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	    panic(err)
        }
    }()

    // -- update
    id := *keyInfo.AccessKeyId
    canCreateBucket := true
    updateKeyRequest := *garage.NewUpdateKeyRequest()
    updateKeyRequest.SetName("openapi-key-updated")
    updateKeyRequest.SetAllow(garage.UpdateKeyRequestAllow { CreateBucket: &canCreateBucket })
    update, r, err := client.KeyApi.UpdateKey(ctx).Id(id).UpdateKeyRequest(updateKeyRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `KeyApi.UpdateKey`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("Updated %v with key name %v\n", *update.AccessKeyId, *update.Name)

    // -- list
    keyList, r, err := client.KeyApi.ListKeys(ctx).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `KeyApi.ListKeys`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("Keys count: %v\n", len(keyList))

    // Bucket
    fmt.Println("\n--- bucket ---")
    // -- create
    global_name := "global-ns-openapi-bucket"
    local_name := "local-ns-openapi-bucket"
    bucketInfo, r, err := client.BucketApi.CreateBucket(ctx).CreateBucketRequest(garage.CreateBucketRequest{
        GlobalAlias: &global_name,
        LocalAlias: &garage.CreateBucketRequestLocalAlias {
            AccessKeyId: keyInfo.AccessKeyId,
	    Alias: &local_name,
        },
    }).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BucketApi.CreateBucket`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("Bucket id: %s\n", *bucketInfo.Id)

    // -- delete
    defer func() {
        r, err := client.BucketApi.DeleteBucket(ctx).Id(*bucketInfo.Id).Execute()
        if err != nil {
            fmt.Fprintf(os.Stderr, "Error when calling `BucketApi.DeleteBucket`\n")
            fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	    panic(err)
        }
    }()

    // -- update
    updateBucketRequest := *garage.NewUpdateBucketRequest() 

    website := garage.NewUpdateBucketRequestWebsiteAccess()
    website.SetEnabled(true)
    website.SetIndexDocument("index.html")
    website.SetErrorDocument("errors/4xx.html")
    updateBucketRequest.SetWebsiteAccess(*website)

    quotas := garage.NewUpdateBucketRequestQuotas()
    quotas.SetMaxSize(1000000000)
    quotas.SetMaxObjects(999999999)
    updateBucketRequest.SetQuotas(*quotas)

    updatedBucket, r, err := client.BucketApi.UpdateBucket(ctx).Id(*bucketInfo.Id).UpdateBucketRequest(updateBucketRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BucketApi.UpdateBucket`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("Bucket %v website activation: %v\n", *updatedBucket.Id, *updatedBucket.WebsiteAccess)

    // -- list
    bucketList, r, err := client.BucketApi.ListBuckets(ctx).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BucketApi.ListBuckets`\n")
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	panic(err)
    }
    fmt.Printf("Bucket count: %v\n", len(bucketList))
}
