const garage = require('garage_administration_api_v1garage_v0_9_0');

const api = new garage.ApiClient("http://127.0.0.1:3903/v1");
api.authentications['bearerAuth'].accessToken = "s3cr3t";

const [node, layout, key, bucket] = [
  new garage.NodesApi(api),
  new garage.LayoutApi(api),
  new garage.KeyApi(api),
  new garage.BucketApi(api),
];

node.getNodes().then((data) => {
  console.log(`nodes: ${Object.values(data.knownNodes).map(n => n.hostname)}`)
}, (error) => {
  console.error(error);
});
