module garage-admin-example

go 1.18

require (
	git.deuxfleurs.fr/garage-sdk/garage-admin-sdk-golang v0.0.0-20231128142223-9dd59cf12c08 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
